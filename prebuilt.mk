LOCAL_DEVICE := $(lastword $(subst _, ,$(TARGET_PRODUCT)))

# App
PRODUCT_PACKAGES += \
    AuroraDroid \
    Bromite \
    BromiteWebView \
    NewPipe

# Overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/prebuilt/overlay/

# Priv-app
PRODUCT_PACKAGES += \
    AuroraServices

$(call inherit-product, vendor/prebuilt/prebuilt-blobs.mk)
